import fs from "node:fs";
import { parse } from 'csv-parse';

const getPosts = async (): Promise<Post[]> => {
    const posts = [];
    console.log(process.cwd())
    const parser = fs
        .createReadStream(`./data/bash-org-quotes.tsv`)
        .pipe(parse({
            delimiter: "\t",
            relax_quotes: true,
            trim: true,
            columns: true,
            escape: "\\",
            quote: null,
        }));

    for await (const record of parser) {
        posts.push({
            id: record.ID,
            score: Number(record.Score),
            quote: record.Quote.replaceAll("\\n", "\n")
        });
    }
    return posts;
};

export interface Post {
    id: string,
    score: number,
    quote: string
}

const post_render_count = (Number(process.env["POST_RENDER_COUNT"] ?? "")) || undefined;
console.log("POST_RENDER_COUNT set to", post_render_count);

export const postList = await getPosts().then(
    posts => posts
        .sort((a, b) => b.score - a.score)
        .slice(0, post_render_count)
);

export const posts: { [key: string]: Post } = Object.fromEntries(
    postList.map(p => [p.id, p])
);

