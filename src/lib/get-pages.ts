import { postList, type Post } from "./get-posts";
export const POSTS_PER_PAGE = 69;
export const MAX_PAGES = Math.ceil(postList.length / POSTS_PER_PAGE) - 1;

export interface PageInfo {
    prev: number | undefined,
    next: number | undefined
    current: number,
}


export interface PageData {
    posts: Post[],
    page_info: PageInfo
}

export function getPageInfo(current_page: number): PageData {
    current_page = Number(current_page);

    if (current_page <= 1) {
        const next_page = postList.length > POSTS_PER_PAGE ? 2 : undefined;
        return {
            posts: postList.slice(0, POSTS_PER_PAGE),
            page_info: {
                current: current_page,
                prev: undefined,
                next: next_page
            }
        }
    }

    const current_post = current_page * POSTS_PER_PAGE;

    const page_slice = postList.slice(current_post, current_post + POSTS_PER_PAGE);

    if (current_page >= MAX_PAGES) {
        return {
            posts: page_slice,
            page_info: {
                current: current_page,
                prev: Math.min(current_page - 1, MAX_PAGES),
                next: undefined,
            }
        }
    }


    return {
        posts: page_slice,
        page_info: {
            current: current_page,
            prev: current_page - 1,
            next: current_page + 1,
        }
    }
}
